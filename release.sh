#!/usr/bin/env bash

# preparing git to checkout
git stash

# getting the last tag and checking it out
git pull --tags

#guessing the last tag name
latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)

#checkout the last tag
git checkout tags/$latestTag

#echoing the last tag you just loaded (just to confirm)
git describe
